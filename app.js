const express = require('express')
const app = express()
const port = 5000

const InsuranceType = {
  HEALTH: 'HEALTH',
  LIFE: 'LIFE',
  ACCIDENT: 'ACCIDENT',
};

const PlanType = {
  BASIC: 'BASIC',
  ADDON: 'ADDON'
};

const data = {
  employeesQuote: [
  {
    minAge: 25,
    maxAge: 30,
    premium: '1,000',
    count: 20,
    totalPremium:'20,000',
  },
  {
    minAge: 35,
    maxAge: 35,
    premium: '1,000',
    count: 10,
    totalPremium:'20,000',
  },
  {
    minAge: 35,
    maxAge: 50,
    premium: '1,000',
    count: 5,
    totalPremium:'20,000',
  }],
  parentsQuote: [
  {
    minAge: 25,
    maxAge: 30,
    premium: '1,000',
    count: 30,
    totalPremium: '20,000',
  }
],
  spouseQuote: [
  {
    minAge: 25,
    maxAge: 30,
    premium: '1,000',
    count: 10,
    totalPremium: '20,000',
  }
],
  kidsQuote: [
  {
    minAge: 25,
    maxAge: 30,
    premium: '1,000',
    count: 20,
    totalPremium: '20,000',
  }
],
  employeesQuoteTotal: '20,000',
  parentsQuoteTotal: '20,000',
  spouseQuoteTotal: '20,000',
  kidsQuoteTotal: '20,000',
  quoteTotalPremium: '80,000',
  planName:'Bronze Choice PPO - F',
  planType: PlanType.BASIC,
  insuranceType: InsuranceType.ACCIDENT,
  iconUrl: undefined,
}

const insureData = {
  title: 'Approval Required',
  message: 'Zomato has added 10,000 employees to Bronze Regence Employee Choice PPO',
  label: 'Approve now',
  url: 'https://www.betterplace.co.in/',
};

// Static Files
app.use(express.static('public'));

// Set View's
app.set('views', './views');
app.set('view engine', 'ejs');

// Navigation
app.get('', (req, res) => {
  res.render('insuranceEmail', { ...insureData })
})
app.get('/old', (req, res) => {
  res.render('oldEmail', { ...insureData })
})

app.get('quote', (req, res) => {
  res.render('quoteSummary', { ...data })
})


app.listen(port, () => console.info(`App listening on port ${port}`))